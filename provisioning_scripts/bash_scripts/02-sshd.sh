#!/usr/bin/env bash


###########################################################
#
# IMPORTANT: source the required environment variables
. $1/config_files/.env_variables.sh
#
###########################################################


cp "$1/config_files/sshd_config" "/etc/ssh/sshd_config"


# allow new user to SSH into the box
echo "AllowUsers $USERNAME" >> /etc/ssh/sshd_config


service ssh restart


