#!/bin/bash


###########################################################
# This script is an example how to copy the 'root_bashrc2.sh'
# and 'user_bashrc2.sh' files into their respective home
# directories, rename them, and then append a line to
# the '~/.bashrc' file, which sources the .bashrc2 file.


###########################################################
#
# IMPORTANT: source the required environment variables
. $1/config_files/.env_variables.sh
#
###########################################################


###########################################################
# Copy .bashrc2 into root home folder
echo "-------------------------------------------------------------"
echo "Copying .bashrc2 into /root/.bashrc2 ..."
echo " "
cp $1/config_files/root_bashrc2.sh /root/.bashrc2
echo ". ~/.bashrc2" >> /root/.bashrc
cp $1/config_files/.vimrc /root/


###########################################################
# Copy .bashrc2 into new_user's home folder
echo "-------------------------------------------------------------"
echo "Copying .bashrc2 into /home/$USERNAME/.bashrc2 ..."
echo " "
cp $1/config_files/user_bashrc2.sh /home/$USERNAME/.bashrc2
chown $USERNAME:$USERNAME /home/$USERNAME/.bashrc2
echo ". ~/.bashrc2" >> /home/$USERNAME/.bashrc
cp $1/config_files/.vimrc /home/$USERNAME/
chown $USERNAME:$USERNAME /home/$USERNAME/.vimrc


