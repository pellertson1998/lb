#!/bin/bash


###########################################################
#
# IMPORTANT: source the required environment variables
. $1/config_files/.env_variables.sh
#
###########################################################


###########################################################
# Install essential packages using apt-get
apt-get update
apt-get install -y git build-essential
apt-get install --upgrade -y vim


###########################################################
# Install VirtualBox Guest Additions, required to
# forward a port to the guest OS.
echo deb http://ftp.debian.org/debian stretch-backports main contrib > /etc/apt/sources.list.d/stretch-backports.list
apt-get update
apt-get install -y virtualbox-guest-dkms virtualbox-guest-x11 linux-headers-$(uname -r)


###########################################################
# Uncomment the following line and add whatever software packages
# you want to be installed when the vagrant box is provisioned.
# This script will be executed when `vagrant up` is run for the
# first time or when `vagrant provision` is executed.
# apt-get install -y SOFTWARE_PACKAGE_1 SOFTWARE_PACKAGE_2





