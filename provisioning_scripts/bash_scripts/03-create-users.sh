#!/bin/bash


###########################################################
#
# IMPORTANT: source the required environment variables
. $1/config_files/.env_variables.sh
#
###########################################################


###########################################################
# You can modify the following command to create user
# accounts in the guest OS. The following command is an
# example of creating a new user account, which sudo permissions
# in a linux guest operating system.  For other operating systems
# you will need to consult the appropriate documentation.
useradd --home-dir /home/$USERNAME --skel /etc/skel --create-home --shell /bin/bash --user-group --groups sudo $USERNAME
mkdir /home/$USERNAME/.ssh
cp "$1/config_files/authorized_keys" "home/$USERNAME/.ssh/"
chmod 600 "/home/$USERNAME/.ssh/authorized_keys"


