set tabstop=4 shiftwidth=4 expandtab
set nocompatible              " be iMproved, required
filetype off                  " required
syntax on
set paste
set number
colorscheme desert

