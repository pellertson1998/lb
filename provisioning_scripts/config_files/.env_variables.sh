#!/usr/bin/env bash


###########################################################
# IMPORTANT: you must change the SITE_NAME variable to the
# fully-qualified domain name of your web site
#
export BOX_BASE="debian/stretch64"
export BOX_HOSTNAME="hostname"
export BOX_MEMORY="4096"
export BOX_CPUS=4
export USERNAME="NEW_USER"
export USER_HOME="/home/$USERNAME"
export SITE_NAME="example.com"
export CPU_CORES=$(grep -c ^processor /proc/cpuinfo)
#
# The following line must not be deleted!
export DEBIAN_FRONTEND=noninteractive
#
###########################################################



###########################################################
# IMPORTANT: You can add any additional variables here

#
###########################################################

