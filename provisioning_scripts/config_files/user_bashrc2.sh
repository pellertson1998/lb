#!/usr/bin/env bash
# ~/.bashrc: executed by bash(1)

###########################################################
# This file is an example of how you could configure
# environment variables for a regular user in the .bashrc
# file. 
#
# NOTE: this file is identical to 'root_bashrc2.sh'
# except for the command prompt on line 22 below, and
# the last 4 lines, which enable Python virtual
# environments.

###########################################################
# terminal safe color codes
###########################################################
WHITE="\[$(tput sgr0; tput bold setaf 7)\]"
CYAN="\[$(tput sgr0; tput setaf 6)\]"
PURPLE="\[$(tput sgr0; tput setaf 5)\]"
BLUE="\[$(tput sgr0; tput setaf 4)\]"
BLUE_BOLD="\[\e[1;34m\]"
GREEN="\[$(tput sgr0; tput setaf 2)\]"
RED="\[$(tput sgr0; tput setaf 1)\]"
BLACK="\[$(tput sgr0; tput setaf 0)\]"
GREY="\[$(tput sgr0; tput setaf 8)\]"
ORANGE="\[$(tput sgr0; tput setaf 3)\]"
CLEAR="\[$(tput sgr0)\]"
export PS1="$BLUE_BOLD\w\n$GREEN\u$GREY@$ORANGE\h$GREY$CLEAR $ "
export LS_COLORS=$LS_COLORS:'di=1;94:ex=0;31:fi=0;32:ln=32:*.tar=35:*.dmg=35:*.zip=35:*.svgz=35:*.tgz=35:*.gz=35:*.sh=90:*.profile=90:*.bashrc=90:*_history=90:*.swp=90:*.lock=90:*.bash_logout=90:*.autoenv_authorized=90:*.txt=0;33:*.ogv=0;33:*.log=33:*.odg=33:*.pdf=33:*.csv=33:'  # Add colors to ls output

###########################################################
# aliases
###########################################################
alias c='clear'
alias ll='ls -hlF --color=always'
alias lc='c; la'
alias la='ls -ahlA --color=always'
alias lca='c; la'
alias l='ls -hCFl --color=always'
alias less='/usr/bin/less -R'
alias ld='ls -dlh ./*/'
alias rb='source ~/.bashrc2'
alias eb='vi ~/.bashrc2'
export HIDDEN='(?<=\d\d:\d\d)\s\W\[\d;\d\dm\..*'
alias hid='la -lah | grep -P $HIDDEN'
alias adog='git log --all --decorate --oneline --graph'

###########################################################
# add color to man pages and less
###########################################################
export LESS="--RAW-CONTROL-CHARS"
export MANPAGER='less -s -M +Gg'
export LESS_TERMCAP_mb=$'\e[1;32m'
export LESS_TERMCAP_md=$'\e[1;32m'
export LESS_TERMCAP_me=$'\e[0m'
export LESS_TERMCAP_se=$'\e[0m'
export LESS_TERMCAP_so=$'\e[01;33m'
export LESS_TERMCAP_ue=$'\e[0m'
export LESS_TERMCAP_us=$'\e[1;4;31m'

###########################################################
# Configure Python virtual environments
###########################################################
export VIRTUALENVWRAPPER_PYTHON=`which python3`
source /usr/local/bin/virtualenvwrapper.sh
