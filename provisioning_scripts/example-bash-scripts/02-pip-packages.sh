#!/bin/bash

###########################################################
# This script illustrates one way to install software packages
# for Python virtual environments.  And it installs all Python
# packages specified by you in the 'requirements.txt' file
# located in this project's root directory.

###########################################################
# Install required packages for pip
apt-get install -y libpython3-dev libpython3-stdlib
apt-get install -y python3-pip

###############################################################
# Install pip packages for virtual Python environments
pip3 install virtualenv virtualenvwrapper

###############################################################
# Install all PyPi packages in requirements.txt
pip3 install -r $1/requirements.txt
