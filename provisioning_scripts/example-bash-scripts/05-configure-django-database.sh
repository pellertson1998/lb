#!/bin/bash

# create postgres database and modify its permissions
su - postrgres
psql
create database cybertronninja;
create user django with password 'whiskeytangofoxtrot';
ALTER ROLE django SET client_encoding TO 'utf8';
ALTER ROLE django SET default_transaction_isolation TO 'read committed';
ALTER ROLE django SET timezone TO 'UTC';
GRANT ALL PRIVILEGES ON DATABASE cybertronninja TO django;
ALTER USER django CREATEDB;
\q
exit;

# make postgres listen to all IP addresses
export BLAH="blah"
regex='^listen_addresses.*$'
replace="listen_addresses = \'\*\'"
awk "{gsub($regex, $replace); {print} }" /etc/postgresql/9.6/main/postgresql.conf

echo "host all all 0.0.0.0/0 md5" >> /etc/postgresql/9.6/main/pg_hba.conf

# restart postgres
service postgresql restart

