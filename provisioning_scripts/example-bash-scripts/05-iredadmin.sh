#!/bin/bash


###########################################################
#
# IMPORTANT: source the required environment variables
. $1/config_files/.env_variables.sh
#
###########################################################

cd /vagrant
wget https://bitbucket.org/zhb/iredmail/downloads/iRedMail-0.9.9.tar.bz2
tar xjf ./iRedMail-0.9.9.tar.bz2

