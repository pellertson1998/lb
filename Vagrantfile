# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure("2") do |config|
	puts "---------------------------------------"
	puts "ARGV = " + ARGV.join(', ')
	puts "---------------------------------------"

	# local directories
	local_dir = File.dirname(__FILE__)
	local_scripts_dir = local_dir + "/provisioning_scripts"
	local_bash_scripts_dir = local_scripts_dir + "/bash_scripts"

	# remote directories
	remote_scripts_dir = "/vagrant/provisioning_scripts"
	remote_bash_scripts_dir = remote_scripts_dir + "/bash_scripts"

	# bash scripts to provision the box
	files = Dir.entries(local_bash_scripts_dir).select {|f| !File.directory? f}
	files = files.sort
	is_provisioned = provisioned?()
	provisioning_complete = false

	all_files = Dir.entries(local_scripts_dir).select {|f| !File.directory? f}
	all_files.each() do |file|
		puts "file = " + file
	end
	# copy provisioning scripts to the guest OS
	config.vm.provision "file", source: local_scripts_dir, destination: remote_scripts_dir

	###############################################################
	# Vagrant base box name
	config.vm.box = "debian/stretch64"

	###############################################################
	# Hostname, Network, and Port Forwarding
	config.vm.hostname = "hostname"
	config.vm.network "public_network", ip: "192.168.0.173", bridge: ["eno1", "eno2", "wlo1", "enp5s0"]
	# config.vm.network "forwarded_port", guest: 5432, host: 5432

	###############################################################
	# Provider-specific configuration
	config.vm.provider "virtualbox" do |vb|
		# Display the VirtualBox GUI when booting the machine
		vb.gui = false

		# Customize the amount of memory on the VM:
		vb.memory = "4096"

		# Number of CPU cores for this VM
		vb.cpus = 4
	end

	###############################################################
	# Provision packages in the VM
	config.vm.provision "shell", inline: <<-SHELL
		export DEBIAN_FRONTEND=noninteractive
	SHELL

	###############################################################
	# Execute BASH provisioning scripts in the guest OS
	config.trigger.after :up do |trigger|
		if ((not is_provisioned) && (not provisioning_complete))
			if (files.length > 0)
				# execute all the bash scripts
				trigger.name = "The box is now being provisioned...\n\nExecute BASH scripts..."
				msg = "BASH files that will be executed: \n\t" + files.join("\n\t") + "\n\n"
				bash_code = "#!/bin/bash\n"
				files.each do |file|
					bash_code = bash_code + "chmod ugo+x " + remote_bash_scripts_dir + "/" + file + ";\n"
					cmd = remote_bash_scripts_dir + "/" + file + " " + remote_scripts_dir
					bash_code += "echo \"---------------------------------------\"\n"
					bash_code += "echo \"Executing: " + cmd + "\"\n"
					bash_code = bash_code + "sudo " + cmd + ";\n"
				end
				msg += "---------------------------------------\n"
				msg += "Bash code to be executed:\n"
				msg += "---------------------------------------\n"
				msg += bash_code
				msg += "---------------------------------------\n\n"
				trigger.warn = msg
				trigger.run_remote = {inline: bash_code}
				provisioning_complete = true
			else
				trigger.warn = "Provisioning did not execute, due to no BASH scripts."
			end
		else
			trigger.warn = "Provisioning did not execute, due to already having been provisioned."
		end
	end
end


###############################################################
# Test if this vagrant box has been provisioned
def provisioned?(vm_name = 'default', provider = 'virtualbox')
	File.exists?(File.join(File.dirname(__FILE__), ".vagrant/machines/#{vm_name}/#{provider}/action_provision"))
end


###############################################################
# Test if this vagrant box has been provisioned
def explicit_provisioning?()
	(ARGV.include?("reload") && ARGV.include?("--provision")) || ARGV.include?("provision")
end

