# Overview
This Vagrantfile allows you to easily provision a new Vagrant box and install any software you like.  
By adding your own Bash scripts and configuration files to this Git repo before running `vagrant up`, 
you can automatically install and configure any software in a newly provisioned Vagrant box.

This Git repo is intended to be used as a starting point for any project needing a Vagrant box.
Its intended use is to first clone this Git repo, add your own custom Bash scripts and configuration files, 
and then run `vagrant up`, which will spin up a new Vagrant box, copy the configuration files to the Vagrant box
and then execute, in order, the Bash scripts inside the new Vagrant box with `sudo su` permissions.

In this README file, the root directory of this Git repo will be referenced as `<PROJECT_DIR>`


# Adding Your Own Scripts
All bash scripts placed in the directory `<PROJECT_DIR>/provisioning_scripts/bash_scripts/` will be executed in alphabetical order. 
The execution order of any custom bash scripts can be controlled by prefixing them with `001`, `002`, `003`, and so on.
All bash scripts are executed with `sudo su` permissions, so the bash scripts can modify any file in the Vagrant box and 
in any directory, including `/etc`, `/root`, or any other directory.  

Every Bash script will be passed one, and only one, argument when it is executed inside the Vagrant box.  
The `$1` argument passed to each Bash script will contain the fully qualified directory inside the Vagrant box, where
all files from `<PROJECT_DIR>/provisioning_scripts/` are copied into.  

For an example of how to use the `$1` argument inside a custom Bash scrip, see the section titled
**Example - Automatically Configure an Apache Site**, below. 
 

# Copying Additional Files Into the Vagrant Box
Optionally, additional "config files" can be copied into the Vagrant box when the box is provisioned.  
Any file placed in `<PROJECT_DIR>/provisioning_scripts/config_files/` will be copied into the Vagrant 
box when it is provisioned.  The "config files" will be copied into the directory 
`/Vagrant/provisioning_scripts/` inside the newly provisioned Vagrant box.

**NOTE:** Even though any files copied into the Vagrant box are called "config files", they are just regular files, so 
you can copy any type of file into the newly provisioned Vagrant box including: installers, binary files, 
images, HTML files, or any other type of file.


# Example - Automatically Configure an Apache Site
Let's say you have developed an Apache site and its configuration in a file named `https-example.com.conf`.
To automatically configure the site you do the following:
- Put the file `https-example.com.conf` into `<PROJECT_DIR>/provisioning_scripts/config_files/`.  It will 
copied into `/Vagrant/provisioning_scripts/config_files` in the newly provisioned Vagrant box.
- Add the following Bash command to a Bash script in `<PROJECT_DIR>/provisioning_scripts/bash_scripts/`:
```bash
cp $1/config_files/https-example.com.conf /etc/apache2/sites-available
a2ensite https-example.com.conf
service apache2 restart
```
- In a Bash terminal, execute the following commands:
```bash
cd <PROJECT_DIR>
Vagrant up 
```

And viola!  Your Apache site will be automatically configured every time the Vagrant box is provisioned.

